from django.db import models



class SMSLog(models.Model):

    name = models.CharField(
        max_length=255, 
        verbose_name="Name",
        blank=True,
        null=True,
    )


    response = models.TextField(
        verbose_name = u'Response',
    )

    code = models.BigIntegerField(
        verbose_name = u'Status Code',
    )

    dateCreated = models.DateTimeField(
        verbose_name = u'Date Created',
        auto_now_add=True,
        blank=True,
        null=True,
    )

    def __str__(self):
        return str(self.id)


    class Meta:
        verbose_name = u'SMS Log'
        verbose_name_plural = u'SMS Logs'
