
from django.conf.urls import *
from django.contrib import admin
from mego import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^m3g0-4dm1n/', admin.site.urls),
    # url(r'^', include('web.urls')),
    url(r'^', include('google_analytics.urls')),
    url(r'^api/', include('api.urls')),
    url(r'^product/', include('products.urls')),
    url(r'^explorer/', include('explorer.urls')),
    url(r'^s/', include('django_short_url.urls', namespace='django_short_url')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

