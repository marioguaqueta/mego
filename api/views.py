import decimal
import json
import random

import requests
import unidecode
from config.models import *
from django.contrib.auth import authenticate
from django.contrib.auth.models import Group
from django.contrib.gis.geos import Point
from django.db.models import Max
from django.http.response import Http404, HttpResponse
# Django
from django.shortcuts import get_list_or_404, get_object_or_404, render
from django_short_url.views import get_surl
from mego.local_settings import *
# FCM
from fcm_django.models import FCMDevice
# Geolocation
from geopy.geocoders import Nominatim
from products.models import *
from rest_framework import viewsets
from rest_framework.authtoken.models import Token
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
# DRF
from rest_framework.views import APIView
from rest_framework_api_key.permissions import HasAPIKey
# Models
from users.models import *

# Serializers
from api.serializers import *
# Utils
from api.util_functions import *


class RegisterUser(APIView):
    permission_classes = [HasAPIKey]

    def post(self, request):
        serializer = RegisterSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        phone = serializer.validated_data['phone']
        device_token = serializer.validated_data['device_token']
        device_type = serializer.validated_data['device_type']
        user_group = serializer.validated_data['user_group']
        referral_code = serializer.validated_data['referral_code']
        user = ''
        code = ''
        try:
            user = User.objects.get(username = phone)    
            user = authenticate(username=phone, password=USER_PASS)
            code = ''.join([str(random.randint(100, 999)).zfill(1) for _ in range(1)]) + ''.join([str(random.randint(10, 99)).zfill(1) for _ in range(1)])
            
            userCode = UserCodes(
                user = user,
                code = code,
                codeType = get_object_or_404(CodeType, name=CONFIRM_CODE,),
                expiresIn = getCurrentDateTimeLambda(30)
            )
            userCode.save()

            if device_token:
                fcm, created = FCMDevice.objects.get_or_create(
                    user = user,
                    registration_id = device_token,
                    type = device_type
                )
                fcm.save()
        except User.DoesNotExist:
            user = User(
                username=phone,
                phone=phone,
                is_active = True,
                referralCode = phone + get_random_string(5),
                freeShipping = FREE_FRIST_ORDER

            )
            user.set_password(USER_PASS)
            user.save()
            user_group = Group.objects.get(name=user_group) 
            user_group.user_set.add(user)
            
            code = ''.join([str(random.randint(100, 999)).zfill(1) for _ in range(1)]) + ''.join([str(random.randint(10, 99)).zfill(1) for _ in range(1)])
            
            userCode = UserCodes(
                user = user,
                code = code,
                codeType = get_object_or_404(CodeType, name=CONFIRM_CODE,),
                expiresIn = getCurrentDateTimeLambda(30)
            )
            userCode.save()

            if referral_code:
                userReferred = UserReferred(
                    userReferred = user,
                    userReferrer = get_object_or_404(User, referralCode=referral_code),
                )
                userReferred.save()

            token = Token.objects.create(user = user)
            if device_token:
                fcm = FCMDevice(
                    user = user,
                    registration_id = device_token,
                    type = device_type
                )
                fcm.save()

        response = {
            'address': AddressSerializer(Address.objects.filter(user = user), many=True, context={"request": request}).data,
            'token': user.auth_token.key,
            'user': UserSerializer(user,).data,
            'code': code,
            'serverDateTime': datetime.now(),
            'categories': CategorySerializer(StoreCategory.objects.filter(status = True).order_by('priority'), many=True, context={"request": request}).data,
            'generalConfig': getGeneralConfig(request)
        }
        # sendSMS(phone, SMS_CODE_MESSAGE.format(code), SMS_SINGLE)   
        sendSMS(phone, code)      


        return Response(response)

        

class AuthUser(APIView):
    permission_classes = [HasAPIKey]

    def post(self, request):
        serializer = AuthenticateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        phone = serializer.validated_data['phone']
        device_token = serializer.validated_data['device_token']
        device_type = serializer.validated_data['device_type']

        user = authenticate(username=phone, password=USER_PASS)
        code = ''.join([str(random.randint(100, 999)).zfill(1) for _ in range(1)]) + ''.join([str(random.randint(10, 99)).zfill(1) for _ in range(1)])
        
        userCode = UserCodes(
            user = user,
            code = code,
            codeType = get_object_or_404(CodeType, name=CONFIRM_CODE,),
            expiresIn = getCurrentDateTimeLambda(30)
        )
        userCode.save()

        if device_token:
            fcm, created = FCMDevice.objects.get_or_create(
                user = user,
                registration_id = device_token,
                type = device_type
            )
            fcm.save()
        

        response = {
            'address': AddressSerializer(Address.objects.filter(user = user), many=True, context={"request": request}).data,
            'token': user.auth_token.key,
            'serverDateTime': datetime.now(),
            'code': code,
            'user': UserSerializer(user,).data,
            'categories': CategorySerializer(StoreCategory.objects.filter(status = True).order_by('priority'), many=True, context={"request": request}).data,
            'generalConfig': getGeneralConfig(request),   
        }


        # sendSMS(phone, SMS_CODE_MESSAGE.format(code), SMS_SINGLE)   
        sendSMS(phone, code)      

        return Response(response)




class AuthTokenUser(APIView):
    permission_classes = [HasAPIKey, IsAuthenticated]

    def get(self, request):
        user = request.user
        response = {
            'address': AddressSerializer(Address.objects.filter(user = user), many=True, context={"request": request}).data,
            'token': user.auth_token.key,
            'serverDateTime': datetime.now(),
            'user': UserSerializer(user,).data,
            'categories': CategorySerializer(StoreCategory.objects.filter(status = True).order_by('priority'), many=True, context={"request": request}).data,
            'generalConfig': getGeneralConfig(request),   
        }

        return Response(response)


class UserProfile(APIView):
    permission_classes = [HasAPIKey, IsAuthenticated]

    def post(self, request):
        serializer = UserProfileSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        phone = serializer.validated_data['phone']
        name = serializer.validated_data['name']
        email = serializer.validated_data['email']
        user = request.user

        if name is not None:
            user.first_name = name
        if email is not None:
            user.email = email
        user.save()

        response = {
            'address': AddressSerializer(Address.objects.filter(user = user), many=True, context={"request": request}).data,
            'token': user.auth_token.key,
            'serverDateTime': datetime.now(),
            'user': UserSerializer(user,).data,
            'categories': CategorySerializer(StoreCategory.objects.filter(status = True).order_by('priority'), many=True, context={"request": request}).data,
            'generalConfig': getGeneralConfig(request),   
        }
        return Response(response)


class UserAddress(APIView):
    permission_classes = [HasAPIKey, IsAuthenticated]

    def post(self, request):
        serializer = UserAddressSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        address = serializer.validated_data['address']
        id = serializer.validated_data['id']
        building = serializer.validated_data['building']
        number = serializer.validated_data['number']
        city = serializer.validated_data['city']
        lat = serializer.validated_data['lat']
        lng = serializer.validated_data['lng']
        user = request.user


        if int(id) > 0 :
            add = get_object_or_404(Address, id = id)
            add.address = address
            add.buildingName = building
            add.unityNumber = number
            add.save()
        else:
            geolocator = Nominatim(user_agent="bravefast")
            location = geolocator.reverse("{0}, {1}".format(lat, lng), language='es')
            data = location.raw
            data = data['address']
            if not city :
                city = "SOGAMOSO"
            try:
                if data['city'] is not None:
                    city = unidecode.unidecode(data['city'])
            except:
                try:
                    if data['county'] is not None:
                        city = unidecode.unidecode(data['county'])
                except:
                    city = "SOGAMOSO"
                
            cityObj, created = City.objects.get_or_create(name= city.upper())
            add = Address(
                user = user,
                city = cityObj,
                address = address,
                buildingName = building,
                unityNumber = number,
                addressType = get_object_or_404(AddressType, id=1),
                latitude = decimal.Decimal(lat),
                longitude = decimal.Decimal(lng)
            )
            add.save()


        response = {
            'address': AddressSerializer(Address.objects.filter(user = user), many=True, context={"request": request}).data,
            'token': user.auth_token.key,
            'serverDateTime': datetime.now(),
            'user': UserSerializer(user,).data,
            'categories': CategorySerializer(StoreCategory.objects.filter(status = True).order_by('priority'), many=True, context={"request": request}).data,
            'generalConfig': getGeneralConfig(request),   
        }
        return Response(response)


class UserComments(APIView):
    permission_classes = [HasAPIKey, IsAuthenticated]

    def post(self, request):
        serializer = UserCommentsSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        comments = serializer.validated_data['comments']
        name = serializer.validated_data['name']
        email = serializer.validated_data['email']
        user = request.user

        user.first_name = name
        user.email = email
        user.save()

        comment = UserContacts(
            user = user,
            comments = comments,
            )
        comment.save()


        response = {
            'address': AddressSerializer(Address.objects.filter(user = user), many=True, context={"request": request}).data,
            'token': user.auth_token.key,
            'serverDateTime': datetime.now(),
            'user': UserSerializer(user,).data,
            'categories': CategorySerializer(StoreCategory.objects.filter(status = True).order_by('priority'), many=True, context={"request": request}).data,
            'generalConfig': getGeneralConfig(request),   
        }
        return Response(response)


class GetNewCode(APIView):
    permission_classes = [HasAPIKey, IsAuthenticated]

    def get(self, request):
        user = request.user

        code = ''.join([str(random.randint(100, 999)).zfill(1) for _ in range(1)]) + ''.join([str(random.randint(10, 99)).zfill(1) for _ in range(1)])
        userCode = UserCodes(
            user = user,
            code = code,
            codeType = get_object_or_404(CodeType, name=CONFIRM_CODE,),
            expiresIn = getCurrentDateTimeLambda(30)
        )
        userCode.save()


        response = {
            'code': code
        }
        
        sendSMS(request.user.phone, code)     

        return Response(response)


class GetPromotions(APIView):
    permission_classes = [HasAPIKey, IsAuthenticated]

    def get(self, request):
        return Response(PromotionSerializer(Promotion.objects.filter(status = True, expiresIn__gt=datetime.now()), many=True, context={"request": request}).data)

# funtion to retrieve categories, stores and products for ios 
class GetStoresAndCategories(APIView):
    permission_classes = [HasAPIKey]

    def get(self, request):
        response = {
            'categories': CategorySerializer(StoreCategory.objects.filter(status = True).order_by('priority'), many=True, context={"request": request}).data,
            'stores': StoreSerializer(Store.objects.filter(is_active = True, city = City.objects.filter(name="SOGAMOSO")[0]).order_by('priority'), many=True, context={"request": request}).data,
            'products': StoreProductsSerializer(Product.objects.filter(is_active = True, store__isnull=False).order_by('priority'), many=True, context={"request": request}).data
        }
        return Response(response)


class GetProductDetailNoLogged(APIView):
    permission_classes = [HasAPIKey]
    def get(self, request,  *args, **kwargs):
        
        instance = Product.objects.filter(is_active = True, id = kwargs.get('id', '1')).order_by('priority')
        print(instance)
        # serializer = self.serializer_class(instance, many=True, context={"request": request})
        productAddition = ProductAdditionsSerializers(ProductAddition.objects.filter(is_active = True, product__id=kwargs.get('id', '1')).order_by('addition__additionType__priority'), many=True, context={"request": request}).data
        productImages = ProductImagesSerializers(ProductImages.objects.filter(isActive = True, product__id=kwargs.get('id', '1')), many=True, context={"request": request}).data
        productStore = StoreSerializer(instance[0].store, many=False, context={"request": request}).data
        response = {
            'productAddition': productAddition,
            'productImages': productImages,
            'store': productStore,
            
        }
        return Response(response)



class BasicPagination(PageNumberPagination):
    page_size_query_param = 10

class GetCategoryStores(APIView):
    pagination_class = BasicPagination
    serializer_class = CategorySerializer
    def get(self, request, format=None, *args, **kwargs):
        instance = StoreCategory.objects.filter(status = True).order_by('priority')
        serializer = self.serializer_class(instance, many=True)
        return Response(serializer.data)


class GetStoresByCategory(APIView):
    pagination_class = BasicPagination
    serializer_class = StoreSerializer
    permission_classes = [HasAPIKey, IsAuthenticated]
    def get(self, request,  *args, **kwargs):
        addr = Address.objects.filter(id = kwargs.get('aid', '1'))
        city = City.objects.filter(name="SOGAMOSO")[0]
        currentTime = datetime.now().time()
        if(len(addr) > 0):
            city = addr[0].city
        # instance = Store.objects.filter(is_active = True, storeMultiCategory__in = [kwargs.get('id', '1')], city = city, startHour__lte = currentTime, finishHour__gte = currentTime).order_by('priority')
        instance = Store.objects.filter(is_active = True, storeMultiCategory__in = [kwargs.get('id', '1')], city = city).order_by('priority')
        serializer = self.serializer_class(instance, many=True, context={"request": request})
        return Response(serializer.data)

class GetStoresProduct(APIView):
    serializer_class = StoreProductSerializer
    permission_classes = [HasAPIKey, IsAuthenticated]
    def get(self, request,  *args, **kwargs):
        instance = Product.objects.filter(is_active = True, store__id = kwargs.get('id', '1')).order_by('priority')
        serializer = self.serializer_class(instance, many=True, context={"request": request})
        return Response(serializer.data)

class GetProductDetail(APIView):
    permission_classes = [HasAPIKey, IsAuthenticated]
    def get(self, request,  *args, **kwargs):
        
        instance = Product.objects.filter(is_active = True, id = kwargs.get('id', '1')).order_by('priority')
        print(instance)
        # serializer = self.serializer_class(instance, many=True, context={"request": request})
        productAddition = ProductAdditionsSerializers(ProductAddition.objects.filter(is_active = True, product__id=kwargs.get('id', '1')).order_by('addition__additionType__priority'), many=True, context={"request": request}).data
        productImages = ProductImagesSerializers(ProductImages.objects.filter(isActive = True, product__id=kwargs.get('id', '1')), many=True, context={"request": request}).data
        productStore = StoreSerializer(instance[0].store, many=False, context={"request": request}).data
        response = {
            'productAddition': productAddition,
            'productImages': productImages,
            'store': productStore,
            
        }
        return Response(response)


class SearchProducts(APIView):
    permission_classes = [HasAPIKey, IsAuthenticated]
    serializer_class = StoreProductSerializer
    def post(self, request,  *args, **kwargs):
        serializer = SearchProductByName(data=request.data)
        serializer.is_valid(raise_exception=True)
        productName = serializer.validated_data['name']
        instance = Product.objects.filter(is_active = True, name__contains = productName).order_by('priority')
        products = self.serializer_class(instance, many=True, context={"request": request})
        return Response(products.data)


class CreateOrder(APIView):
    permission_classes = [HasAPIKey, IsAuthenticated]
    serializer_class = StoreProductSerializer
    def post(self, request,  *args, **kwargs):
        serializer = ItemsOderSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        items = serializer.validated_data['items']
        paymentMethod = serializer.validated_data['paymentMethod']
        shipPrice = serializer.validated_data['shipPrice']
        store = get_object_or_404(Store, id = items[0]['storeId'],) 
        address = get_object_or_404(Address, id = items[0]['addressId'],) 
        paymentMethod = get_object_or_404(PaymentMethod, id = paymentMethod)
        orderState = get_object_or_404(OrderState, name = ORDER_STATE_NEW)
        user = request.user

        # Create Order
        order = Order(
            store = store,
            clientUser = user,
            address = address,
            paymentMethod = paymentMethod,
            state = orderState,
            shipPrice = shipPrice,
        )

        order.save()

        for item in items:         
            productOrder = ProductOrder(
                order = order,
                product = Product.objects.get(id=item['productId'], is_active = True),
                quantity = item['productQuantity'],
                viewedPrice = item['productPrice'],
                observation = item['observations'],
            )
            productOrder.save()
            for addition in item['additions']:
                productAddition = AdditionOrder(
                    productOrder = productOrder,
                    addition = ProductAddition.objects.get(id=addition['additionId'], is_active = True),
                    quantity = addition['additionQuantity'],
                    viewedPrice = addition['additionPrice'],
                )
                productAddition.save()
        lurl = '/mP5vCd-admin/products/orderviewmodel/mP5vCd-admin/?oid={0}'.format(order.id)
        surl = request.build_absolute_uri(get_surl(lurl))
        sendOrderSMS(surl, store)

        

        return Response({'orderId': order.id, 'creationDate':order.orderDate.strftime("%Y-%m-%dT%H:%M:%S")})




class GetOrderHistory(APIView):
    pagination_class = BasicPagination
    serializer = ProductAdditionOrderHistorySerializer
    permission_classes = [HasAPIKey, IsAuthenticated]
    def get(self, request,  *args, **kwargs):
        orders = Order.objects.filter(clientUser = request.user).order_by('-orderDate').select_related()
        orderInstance = OrderHistorySerializer(orders, many=True, context={"request": request}).data
        productOrders = ProductOrder.objects.filter(order__clientUser = request.user).select_related('order')
        productOrdersInstance = ProductOrderHistorySerializer(productOrders, many=True, context={"request": request}).data
        productAddition = AdditionOrder.objects.filter(productOrder__in = productOrders).select_related('productOrder')
        productAdditionInstance = ProductAdditionOrderHistorySerializer(productAddition, many=True, context={"request": request}).data

        return Response({'products':productOrdersInstance})


class CancelOrder(APIView):
    pagination_class = BasicPagination
    serializer = CancelOrderSerializer
    permission_classes = [HasAPIKey, IsAuthenticated]
    def post(self, request,  *args, **kwargs):
        serializer = CancelOrderSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        orderId = serializer.validated_data['orderId']
        order = Order.objects.filter(id = orderId)
        stateAccepted = get_object_or_404(OrderState, name = ORDER_STATE_ACCEPTED)
        stateCanceled = get_object_or_404(OrderState, name = ORDER_STATE_CANCELED)
        if len(order) > 0:
            order = order[0]
            if order.state == stateAccepted:
                return Response({'detail':MESSAGE_ORDER_STATE_ACCEPTED})
            else:
                order.state = stateCanceled
                order.save()
                lurl = '/mP5vCd-admin/products/orderviewmodel/mP5vCd-admin/?oid={0}'.format(order.id)
                surl = request.build_absolute_uri(get_surl(lurl))
                sendCanceledOrderSMS(surl)
                return Response({'detail':MESSAGE_ORDER_STATE_CANCELED})    
        else:
            return Response({'detail':MESSAGE_ORDER_NOT_FOUND})



# Funtion for general config
def getGeneralConfig(request):
    disc = Product.objects.filter(is_active=True).order_by('-percentageDiscount')
    maxDisc = "-0%"
    if len(disc) > 0:
        maxDisc = disc[0].percentageDiscount
    generalConfig = {
        'paymentMethods': PaymentMethodSerializer(PaymentMethod.objects.all(), many=True, context={"request": request}).data,
        'addressTypes': AddressTypeSerializer(AddressType.objects.all(), many=True, context={"request": request}).data,
        'identificationTypes': IdentificationTypesSerializer(IdentificationType.objects.all(), many=True, context={"request": request}).data,
        'cities': CitiesSerializer(City.objects.all(), many=True, context={"request": request}).data,
        'codeTypes': CodeTypeSerializer(CodeType.objects.all(), many=True, context={"request": request}).data,
        'genders': GenderSerializer(Gender.objects.all(), many=True, context={"request": request}).data,
        'promotions': PromotionSerializer(Promotion.objects.filter(status = True, expiresIn__gt=datetime.now()), many=True, context={"request": request}).data,
        'distances' : DistancePricesSerializer(DistancePrices.objects.all(), many=True, context={"request": request}).data,
        'maxDiscount':'-{}%'.format(maxDisc),
        'banners' : BannersSerializer(Banner.objects.filter(status=True), many=True, context={"request": request}).data,
    }
    return generalConfig






# Administration Views


def accept_order(request):
    if request.method == "POST" and request.is_ajax():
        orderId = request.POST.get("orderId", "")
        order = Order.objects.filter(id = orderId)
        if len(order) > 0:
            order = order[0]
            order.state = get_object_or_404(OrderState, name = ORDER_STATE_ACCEPTED)
            order.save()
            user = order.clientUser
            devices = FCMDevice.objects.filter(user = user)
            devices.send_message(title= ORDER_ACCEPTED_TITLE, body= ORDER_ACCEPTED_MESSAGE)
            return HttpResponse(orderId)
        else:
            return HttpResponse(status=500)
        

def reject_order(request):
    if request.method == "POST" and request.is_ajax():
        orderId = request.POST.get("orderId", "")
        order = Order.objects.filter(id = orderId)
        if len(order) > 0:
            order = order[0]
            order.state = get_object_or_404(OrderState, name = ORDER_STATE_REJECTED)
            order.save()
            user = order.clientUser
            devices = FCMDevice.objects.filter(user = user)
            devices.send_message(title= ORDER_REJECTED_TITLE, body= ORDER_REJECTED_MESSAGE)
            return HttpResponse(orderId)
        else:
            return HttpResponse(status=500)  
