
from datetime import datetime
from users.models import UserInteractions
from re import sub
from rest_framework.authtoken.models import Token


class BaseMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        return self.get_response(request)

class ProcessViewNoneMiddleware(BaseMiddleware):
    def process_view(self, request, view_func, view_args, view_kwargs):
        views = {"app_index", "RegisterUser", "AuthUser", "AuthTokenUser", "UserProfile", "UserAddress", "UserComments", "GetNewCode", "GetPromotions", "GetCategoryStores", "GetStoresByCategory", "GetStoresProduct", "GetProductDetail", "SearchProducts", "CreateOrder"}
        try:
            if str(view_func.__name__) in views :
                header_token = request.META.get('HTTP_AUTHORIZATION', None)
                if header_token is not None:
                    key = header_token.replace("Token ", "") 
                    token_obj = Token.objects.get(key = key)
                    interaction = UserInteractions(
                        user = token_obj.user,
                        url = request.build_absolute_uri(),
                        viewName = str(view_func.__name__),
                        requestedData = str(request) + "\n\n" + str(view_args) + "\n\n" + str(view_kwargs),
                    )
                    interaction.save()
        except:
            pass
        return None
    


