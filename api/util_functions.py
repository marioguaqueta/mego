import random
import string
import json
import requests
import base64
import json


from datetime import datetime
from datetime import timedelta
from rest_framework.exceptions import APIException
from django.utils.encoding import force_text
from mego.local_settings import *
from products.models import *
from users.models import *
from logs.models import *
# FCM
from fcm_django.models import FCMDevice

# Twilio
from twilio.rest import Client



def validatePhone(phone):
    return (len(phone) == 10)


def get_random_string(length):
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str


def requestToken():
    headers = {'content-type': 'application/json'}
    payload = json.dumps({'account': SMS_ACCOUNT, 'password': SMS_PASS})
    r = requests.post(SMS_TOKEN_URL, data=payload, headers=headers)
    return r.json()['token']


def sendSMS(phone, code):
    message = SMS_SEND_USERNAME_LABS + ":" + SMS_SEND_LABS
    message_bytes = message.encode('ascii')
    base64_bytes = base64.b64encode(message_bytes).decode("ascii")
    url = SMS_SEND_URL_LABS
    payload = json.dumps({
        "message": SMS_CODE_MESSAGE.format(code),
        "tpoa": "Sender",
        "recipient": [
            {
                "msisdn": "57" + str(phone)
            }
        ]
    })
    headers = {
        'Content-Type': "application/json",
        'Authorization': "Basic %s" % base64_bytes,
        'Cache-Control': "no-cache"
    }
    response = requests.request("POST", url, data=payload, headers=headers)

    sms_log = SMSLog.objects.create(response= json.loads(response.content), code=200, name='Register/Login')




def sendOrderSMS(surl, store):
    PushNewOrder(PUSH_NEW_ORDER_TITLE, PUSH_NEW_ORDER_MESSAGE)
    message = SMS_SEND_USERNAME_LABS + ":" + SMS_SEND_LABS
    message_bytes = message.encode('ascii')
    base64_bytes = base64.b64encode(message_bytes).decode("ascii")
    url = SMS_SEND_URL_LABS

    maxTime = datetime.strptime(MAX_ORDER_RECEIVE_TIME, '%H:%M:%S').time()
    minTime = datetime.strptime(MIN_ORDER_RECEIVE_TIME, '%H:%M:%S').time()
    currentTime = datetime.now().time()
    orderPhones = SMS_ORDER_PHONES
    if (currentTime >= maxTime  or currentTime < minTime):
        orderPhones.append({"msisdn": "57{0}".format(store.phone)})
    payload = json.dumps({
        "message": SMS_ORDER_MESSAGE.format(surl),
        "tpoa": "Sender",
        "recipient": orderPhones
    })
    headers = {
        'Content-Type': "application/json",
        'Authorization': "Basic %s" % base64_bytes,
        'Cache-Control': "no-cache"
    }
    response = requests.request("POST", url, data=payload, headers=headers)
    sms_log = SMSLog.objects.create(response= json.loads(response.content), code=200, name='Create Order Labs')
    client = Client(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN)
    message = client.messages.create(  
        messaging_service_sid='MG917751a635eec1997053ba4ad32c5196', 
        body=SMS_ORDER_MESSAGE.format(surl),      
        to='+573114913693' 
    ) 
    sms_log = SMSLog.objects.create(response= str(message.__dict__), code=200, name='Create Order Twilio')


def sendCanceledOrderSMS(surl):
    PushNewOrder(PUSH_CANCELED_ORDER_TITLE, PUSH_CANCELED_ORDER_MESSAGE)
    message = SMS_SEND_USERNAME_LABS + ":" + SMS_SEND_LABS
    message_bytes = message.encode('ascii')
    base64_bytes = base64.b64encode(message_bytes).decode("ascii")
    url = SMS_SEND_URL_LABS
    payload = json.dumps({
        "message": SMS_ORDER_CANCELED.format(surl),
        "tpoa": "Sender",
        "recipient": SMS_ORDER_PHONES
    })
    headers = {
        'Content-Type': "application/json",
        'Authorization': "Basic %s" % base64_bytes,
        'Cache-Control': "no-cache"
    }
    response = requests.request("POST", url, data=payload, headers=headers)
    sms_log = SMSLog.objects.create(response= json.loads(response.content), code=200, name='Cancel Order Labs')
    client = Client(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN)
    message = client.messages.create(  
        messaging_service_sid='MG917751a635eec1997053ba4ad32c5196', 
        body=SMS_ORDER_MESSAGE.format(surl),      
        to='+573114913693' 
    ) 
    sms_log = SMSLog.objects.create(response= str(message.__dict__), code=200, name='Create Order Twilio')

 



def sendMarketingSMS(phones, messageText):
    message = SMS_SEND_USERNAME_LABS + ":" + SMS_SEND_LABS
    message_bytes = message.encode('ascii')
    base64_bytes = base64.b64encode(message_bytes).decode("ascii")
    url = SMS_SEND_URL_LABS
    payload = json.dumps({
        "message": messageText,
        "tpoa": "Sender",
        "recipient": phones
    })
    headers = {
        'Content-Type': "application/json",
        'Authorization': "Basic %s" % base64_bytes,
        'Cache-Control': "no-cache"
    }
    response = requests.request("POST", url, data=payload, headers=headers)


def getCurrentDateTimeLambda(minutes):
    return datetime.now() + timedelta(minutes=minutes)


class CustomValidation(APIException):
    status_code = 400
    default_detail = 'A server error occurred.'

    def __init__(self, detail, status_code):
        if status_code is not None:
            self.status_code = status_code
        if detail is not None:
            self.detail = {'detail': force_text(detail)}
        else:
            self.detail = {'detail': force_text(self.default_detail)}

def PushNewOrder(title,message):
    push_order_users = User.objects.filter(username__in=PUSH_ORDER_USERS)
    push_order_devices = FCMDevice.objects.filter(user__in = push_order_users)
    push_order_devices.send_message(title= title, body= message)