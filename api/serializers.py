from api.util_functions import *
from rest_framework import serializers
from users.models import *
from products.models import *
from config.models import *
from django.shortcuts import get_object_or_404
from mego.local_settings import *
from django.contrib.auth import authenticate

from datetime import datetime


# User Serializers

class RegisterSerializer(serializers.Serializer):
    phone = serializers.CharField()
    device_token = serializers.CharField(required=False, allow_blank=True, allow_null=True, default=None)
    device_type = serializers.CharField(required=False, allow_blank=True, allow_null=True, default=None)
    user_group = serializers.CharField()
    referral_code = serializers.CharField(required=False, allow_blank=True, allow_null=True, default=None)
    def validate(self, attrs):
        phone = attrs.get('phone')
        device_token = attrs.get('device_token')
        device_type = attrs.get('device_type')
        user_group = attrs.get('user_group')
        referral_code = attrs.get('referral_code')
        if phone and user_group:
            if validatePhone(phone):
                attrs['phone'] = phone
                attrs['device_token'] = device_token
                attrs['device_type'] = device_type
                attrs['user_group'] = user_group
                attrs['referral_code'] = referral_code
                return attrs
                
                # try:
                #     user = User.objects.get(username = phone)
                #     msg = 'Tu usuario ya existe'
                #     # raise CustomValidation(msg, 404)
                # except User.DoesNotExist:
                #     if referral_code:
                #         try:
                #             User.objects.get(referralCode = referral_code)
                #         except User.DoesNotExist:
                #             msg = 'Tu código de referido no existe'
                #             raise CustomValidation(msg, 404)
                #     attrs['phone'] = phone
                #     attrs['device_token'] = device_token
                #     attrs['device_type'] = device_type
                #     attrs['user_group'] = user_group
                #     attrs['referral_code'] = referral_code
                #     return attrs 
            else:
                msg = 'Tu teléfono es invalido'
                raise CustomValidation(msg, 404)
        else:
            msg = 'Opss, tuvimos un error intenta nuevamente'
            raise CustomValidation(msg, 404)


class AuthenticateSerializer(serializers.Serializer):
    phone = serializers.CharField()
    device_token = serializers.CharField(required=False, allow_blank=True, allow_null=True, default=None)
    device_type = serializers.CharField(required=False, allow_blank=True, allow_null=True, default=None)
    user_group = serializers.CharField()
    def validate(self, attrs):
        phone = attrs.get('phone')
        device_token = attrs.get('device_token')
        device_type = attrs.get('device_type')
        user_group = attrs.get('user_group')
        if phone and user_group:
            if validatePhone(phone):
                try:
                    user = User.objects.get(username = phone)
                except User.DoesNotExist:
                    msg = 'Tu usuario no existe'
                    raise CustomValidation(msg, 404)

                user = authenticate(username=phone, password=USER_PASS)
                
                if user:
                    if not user.is_active:
                        msg = ('Tu cuenta ha sido desactivada')
                        raise CustomValidation(msg, 404)
                else:
                    msg = ('Tu cuenta ha sido desactivada')
                    raise CustomValidation(msg, 404)

            else:
                msg = 'Tu teléfono es invalido'
                raise CustomValidation(msg, 404)
        else:
            msg = 'Opss, tuvimos un error intenta nuevamente'
            raise CustomValidation(msg, 404)
        attrs['user'] = user
        return attrs

class UserProfileSerializer(serializers.Serializer):
    phone = serializers.CharField()
    name = serializers.CharField(required=False, allow_blank=True, allow_null=True, default=None)
    email = serializers.CharField(required=False, allow_blank=True, allow_null=True, default=None)
    def validate(self, attrs):
        phone = attrs.get('phone')
        name = attrs.get('name')
        email = attrs.get('email')
        if phone :
            attrs['phone'] = phone
            attrs['name'] = name
            attrs['email'] = email
        else:
            msg = 'Opss, tuvimos un error intenta nuevamente'
            raise CustomValidation(msg, 404)
        return attrs


class UserCommentsSerializer(serializers.Serializer):
    comments = serializers.CharField()
    name = serializers.CharField(required=False, allow_blank=True, allow_null=True, default=None)
    email = serializers.CharField(required=False, allow_blank=True, allow_null=True, default=None)
    def validate(self, attrs):
        comments = attrs.get('comments')
        name = attrs.get('name')
        email = attrs.get('email')
        if comments :
            attrs['comments'] = comments
            attrs['name'] = name
            attrs['email'] = email
        else:
            msg = 'Opss, tuvimos un error intenta nuevamente'
            raise CustomValidation(msg, 404)
        return attrs

class UserAddressSerializer(serializers.Serializer):
    
    address = serializers.CharField()
    id = serializers.CharField(required=False, allow_blank=True, allow_null=True, default=None)
    building = serializers.CharField(required=False, allow_blank=True, allow_null=True, default=None)
    number = serializers.CharField(required=False, allow_blank=True, allow_null=True, default=None)
    city  = serializers.CharField(required=False, allow_blank=True, allow_null=True, default=None)
    lat  = serializers.CharField(required=False, allow_blank=True, allow_null=True, default=None)
    lng  = serializers.CharField(required=False, allow_blank=True, allow_null=True, default=None)
    def validate(self, attrs):
        address = attrs.get('address')
        id = attrs.get('id')
        building = attrs.get('building')
        number = attrs.get('number')
        city = attrs.get('city')
        lat = attrs.get('lat')
        lng = attrs.get('lng')
        if address :
            attrs['address'] = address
            attrs['id'] = id
            attrs['building'] = building
            attrs['number'] = number
            attrs['city'] = city
            attrs['lat'] = lat
            attrs['lng'] = lng
        else:
            msg = 'Opss, tuvimos un error intenta nuevamente'
            raise CustomValidation(msg, 404)
        return attrs




class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        exclude = ('password', 'last_login', 'is_superuser', 'is_staff', 'date_joined', 'groups', 'user_permissions')


class AddressSerializer(serializers.ModelSerializer):

    class Meta:
        model = Address
        depth = 1
        exclude = ('user',)

class AddressTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = AddressType
        depth = 1
        fields = '__all__'


class PaymentMethodSerializer(serializers.ModelSerializer):

    class Meta:
        model = PaymentMethod
        depth = 1
        fields = '__all__'


class DistancePricesSerializer(serializers.ModelSerializer):

    class Meta:
        model = DistancePrices
        depth = 1
        fields = '__all__'



class UserCodeSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserCodes
        depth = 1
        exclude = ('user',)

class IdentificationTypesSerializer(serializers.ModelSerializer):

    class Meta:
        model = IdentificationType
        depth = 1
        fields = '__all__'

class CitiesSerializer(serializers.ModelSerializer):

    class Meta:
        model = City
        depth = 1
        fields = '__all__'


class CodeTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = CodeType
        depth = 1
        fields = '__all__'



class GenderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Gender
        depth = 1
        fields = '__all__'



from mego import settings

# Product Serializers
class CategorySerializer(serializers.ModelSerializer):



    class Meta:
        model = StoreCategory
        fields = '__all__'
        depth = 2
    
    # def get_icon_url(self, obj):
    #     request = self.context.get('request')
    #     icon_url = obj.icon.url
    #     print(icon_url)
    #     return request.build_absolute_uri(icon_url)
    #     # return obj.icon.url


# Stores Serializers
class StoreSerializer(serializers.ModelSerializer):

    maxDiscount = serializers.SerializerMethodField('get_max_discount')
    def get_max_discount(self, obj):
        products = Product.objects.filter(is_active = True, store__id = obj.id)
        maxDiscount = 0
        for product in products:
            if product.percentageDiscount > maxDiscount:
                maxDiscount = product.percentageDiscount
        return maxDiscount

    isOpen = serializers.SerializerMethodField('get_is_open')
    def get_is_open(self, obj):
        currentTime = datetime.now().time()
        curTime = datetime.now()
        openTimes = OpenningTimes.objects.filter(weekday = curTime.weekday(), from_hour__lte = currentTime , to_hour__gte = currentTime)
        store = Store.objects.filter(id = obj.id, oppeningTimes__in = openTimes, is_active = True)
        return len(store)>0
        # return (currentTime >= obj.startHour and currentTime < obj.finishHour)

    openingTime = serializers.SerializerMethodField('get_openning_time')
    def get_openning_time(self, obj):
        currentTime = datetime.now().time()
        curTime = datetime.now()
        openTimes = OpenningTimes.objects.filter(weekday = curTime.weekday(), from_hour__lte = currentTime , to_hour__gte = currentTime)
        store = Store.objects.filter(id = obj.id, oppeningTimes__in = openTimes, is_active = True)
        if len(store) > 0:
            return ""
        else:
            openTimes = OpenningTimes.objects.filter(weekday = curTime.weekday())
            store = Store.objects.filter(id = obj.id, oppeningTimes__in = openTimes, is_active = True)
            if len(store) > 0:
                store = store[0]
                opt = store.oppeningTimes.all()
                initHour = '12:00'
                for time in opt:
                    if time.weekday == curTime.weekday():
                        initHour = time.from_hour
                return "Nuestro aliado se encuentra cerrado.\nAbrirá a las {0}".format(initHour)
            else:
                return "Nuestro aliado se encuentra cerrado por el día de hoy"




    class Meta:
        model = Store
        fields = ('id', 'name', 'description', 'tag', 'priority', 'percentage', 'meanTime', 'startHour', 
        'finishHour', 'is_active', 'free_shipping', 'latitude', 'longitude', 'igUrl', 'fbUrl', 'address', 'icon', 'deliveryType', 
        'storeCategory','storeMultiCategory', 'productCategory', 'maxDiscount', 'isOpen', 'openingTime' )
        depth = 1



class PromotionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Promotion
        fields = '__all__'
        depth = 2


class ProductAdditionsSerializers(serializers.ModelSerializer):

    class Meta:
        model = ProductAddition
        fields = ('id', 'description', 'is_active', 'price', 'addition', )
        depth = 2


class ProductImagesSerializers(serializers.ModelSerializer):

    class Meta:
        model = ProductImages
        fields = ('id', 'isActive', 'icon', )
        depth = 1


class StoreProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        depth = 1
        fields = ('id', 'name','price','percentageDiscount', 'qualify','priority','is_active','icon','shortDescription','description','productCategory',)


class StoreProductsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        depth = 1
        fields = ('id', 'name','price','percentageDiscount', 'qualify','priority','is_active','icon','shortDescription','description','productCategory', 'store')




class SearchProductByName(serializers.Serializer):
    name = serializers.CharField()
    def validate(self, attrs):
        name = attrs.get('name')
        if name :
            attrs['name'] = name
        else:
            msg = 'Opss, tuvimos un error intenta nuevamente'
            raise CustomValidation(msg, 404)
        return attrs


class ItemAddition(serializers.Serializer):
    id = serializers.IntegerField(required=False, allow_null=True, default=None)
    additionId = serializers.IntegerField(required=False, allow_null=True, default=None)
    productId = serializers.IntegerField(required=False, allow_null=True, default=None)
    additionQuantity = serializers.IntegerField(required=False, allow_null=True, default=None)
    additionName = serializers.CharField(required=False, allow_null=True, default=None)
    additionImage = serializers.CharField(required=False, allow_null=True, default=None)
    additionPrice = serializers.IntegerField(required=False, allow_null=True, default=None)
    
    def validate(self, attrs):
        id = attrs.get('id')
        additionQuantity = attrs.get('additionQuantity')
        
        if id and additionQuantity :
            attrs['id'] = id
            attrs['additionQuantity'] = additionQuantity
        else:
            msg = 'Opss, tuvimos un error intenta nuevamente'
            raise CustomValidation(msg, 404)
        return attrs


class ItemOder(serializers.Serializer):
    id = serializers.IntegerField(required=False, allow_null=True, default=None)
    productId = serializers.IntegerField(required=False,  allow_null=True, default=None)
    storeId = serializers.IntegerField(required=False,  allow_null=True, default=None)
    addressId = serializers.IntegerField(required=False,  allow_null=True, default=None)
    productName = serializers.CharField(required=False, allow_null=True, default=None)
    productQuantity = serializers.IntegerField(required=False,  allow_null=True, default=None)
    productImage = serializers.CharField(required=False,  allow_null=True, default=None)
    productPrice = serializers.IntegerField(required=False,  allow_null=True, default=None)
    totalAddition = serializers.IntegerField(required=False,  allow_null=True, default=None)
    additions = serializers.ListField(child=ItemAddition(allow_null=True, required=False, default=None))
    observations = serializers.CharField(required=False,  allow_null=True, default=None)

    def validate(self, attrs):
        productId = attrs.get('productId')
        productQuantity = attrs.get('productQuantity')
        
        if productId and productQuantity :
            attrs['productId'] = productId
            attrs['productQuantity'] = productQuantity
        else:
            msg = 'Opss, tuvimos un error intenta nuevamente'
            raise CustomValidation(msg, 404)
        return attrs

class ItemsOderSerializer(serializers.Serializer):
    items = serializers.ListField(child=ItemOder())
    paymentMethod = serializers.IntegerField(required=False,  allow_null=True, default=None)
    shipPrice = serializers.IntegerField(required=False,  allow_null=True, default=None)


# Order History Models Serializer Specific

class StoreOrderHistorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Store
        depth = 1
        exclude = ('productCategory','storeCategory', 'storeMultiCategory', 'city', 'deliveryType')


class OrderHistorySerializer(serializers.ModelSerializer):
    store = StoreOrderHistorySerializer(read_only = True)
    class Meta:
        model = Order
        depth = 1
        exclude = ('clientUser','deliveredBy', )


class ProductOrderHistorySerializer(serializers.ModelSerializer):
    order = OrderHistorySerializer(read_only=True)
    class Meta:
        model = ProductOrder
        depth = 1
        fields = '__all__'
        # exclude = ('clientUser', )


class ProductAdditionOrderHistorySerializer(serializers.ModelSerializer):

    class Meta:
        model = AdditionOrder
        depth = 1
        fields = '__all__'


class CancelOrderSerializer(serializers.Serializer):
    orderId = serializers.IntegerField()
    def validate(self, attrs):
        orderId = attrs.get('orderId')
        if orderId:
            attrs['orderId'] = orderId
            return attrs
        else:
            msg = 'Opss, tuvimos un error intenta nuevamente'
            raise CustomValidation(msg, 404)



class BannersSerializer(serializers.ModelSerializer):

    class Meta:
        model = Banner
        depth = 1
        fields = '__all__'