from django.conf.urls import *
from api import views

urlpatterns = [
    url(r'^register-user/', views.RegisterUser.as_view(), name='user_registration',),
    url(r'^auth-user/', views.AuthUser.as_view(), name='user_authentication',),
    url(r'^user-profile/', views.UserProfile.as_view(), name='user_profile',),
    url(r'^auth-token-user/', views.AuthTokenUser.as_view(), name='user_token_authentication',),
    url(r'^user-address/', views.UserAddress.as_view(), name='user_address',),
    url(r'^user-comments/', views.UserComments.as_view(), name='user_comments',),
    url(r'^new-code/', views.GetNewCode.as_view(), name='get_new_code',),
    url(r'^promotions/', views.GetPromotions.as_view(), name='promotions',),
    url(r'^store-list/', views.GetCategoryStores.as_view(), name='get_store_list',),
    url(r'^stores-by-category/(?P<id>.+)/(?P<aid>.+)/$', views.GetStoresByCategory.as_view(), name='get_stores_by_category',),
    url(r'^stores-product/(?P<id>.+)/$', views.GetStoresProduct.as_view(), name='get_stores_product',),
    url(r'^product-detail/(?P<id>.+)/$', views.GetProductDetail.as_view(), name='get_product_detail',),
    url(r'^search-product/', views.SearchProducts.as_view(), name='search_products',),
    url(r'^create-order/', views.CreateOrder.as_view(), name='create_order',),
    url(r'^get-order-history/', views.GetOrderHistory.as_view(), name='create_order',),
    url(r'^cancel-order/', views.CancelOrder.as_view(), name='create_order',),

    # iOS
    url(r'^get-categories/', views.GetStoresAndCategories.as_view(), name='get_stores_and_categories',),
    url(r'^product-detail-nologged/(?P<id>.+)/$', views.GetProductDetailNoLogged.as_view(), name='get_product_detail_no_logged',),




    # Administration
    url(r'^accept-order/', views.accept_order, name='accept_order',),
    url(r'^reject-order/', views.reject_order, name='reject_order',),

    
]