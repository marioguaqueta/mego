from django.conf.urls import *
from web import views

urlpatterns = [
    url(r'^$',views.index.as_view(), name = 'index'),
    url(r'^terms/',views.TermsView.as_view(), name = 'Terms'),
    url(r'^policy/',views.PolicyView.as_view(), name = 'Policy'),
    url(r'^download/',views.download.as_view(), name = 'download'),
    url(r'^products/(?P<id>.+)/$', views.getProductStore.as_view(), name='get_product_by_store',),
]