from django.db import models
from django.core.exceptions import ValidationError



class Terms(models.Model):
    
    termsFile = models.FileField(
        verbose_name=u'Terms File',
        upload_to='Terms/TermsFiles/',
        null= True,
    )

    policyFile = models.FileField(
        verbose_name=u'Policy File',
        upload_to='Terms/TermsFiles/',
        null= True,
    )
    def clean(self):
        if Terms.objects.exists() and not self.pk:
            raise ValidationError("Solo se puede tener un archivo de configuración")
    def save(self, *args, **kwargs):
       return super(Terms, self).save(*args, **kwargs)
    def __str__(self): #returns the name of the company
       return str(self.pk)
    
    class Meta: #This will display on the default Django Admin
       verbose_name = 'Configuración'
       verbose_name_plural = 'Configuración'

