from django.shortcuts import render
from django.views.generic import View
from django.shortcuts import render, redirect, get_object_or_404, HttpResponseRedirect
from products.models import *
from .models import *
from django.http import FileResponse, Http404


class index(View):
    
    def get(self, request):
        return render(request, 'index/index.html')


class download(View):
    
    def get(self, request):
        return render(request, 'index/download.html')


class getProductStore(View):
    
    def get(self, request, *args, **kwargs):

        store_id = kwargs.get('id', '1')
        print(store_id)
        store = Store.objects.filter(id = store_id)
        products = Product.objects.filter(store__id = store_id)
        return render(request, 'index/product_list.html', {'store': store[0], 'products': products})


class TermsView(View):
    def get(self, request):
        try:
            config = Terms.objects.all()
            return FileResponse(config[0].termsFile, content_type='application/pdf')
        except FileNotFoundError:
            raise Http404()


class PolicyView(View):
    def get(self, request):
        try:
            config = Terms.objects.all()
            return FileResponse(config[0].policyFile, content_type='application/pdf')
        except FileNotFoundError:
            raise Http404()