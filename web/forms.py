from django import forms
  
from users.models import *
  
# create a ModelForm
class ContactUsForm(forms.ModelForm):
    # specify the name of model to use
    class Meta:
        model = UserContacts
        fields = "__all__"