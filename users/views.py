import json
from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import View
from django.shortcuts import render, redirect, get_object_or_404, HttpResponseRedirect
from products.models import *
from itertools import chain
import datetime
from django.contrib.auth.mixins import LoginRequiredMixin


class SendSMS(LoginRequiredMixin, View):
    
    login_url = '/mP5vCd-admin/'
    def get(self, request):
        return render(request, 'send_sms.html')


class SendPUSH(LoginRequiredMixin, View):
    login_url = '/mP5vCd-admin/'
    def get(self, request):
        return render(request, 'send_push.html')