from django.db import models
from django.contrib.auth.models import AbstractUser, AbstractBaseUser

class User(AbstractUser):
    """Model that overrides authModel to allow has more information in the same table
    """

    phone = models.CharField(
        max_length = 30,
        verbose_name = 'Cellphone',
        )


    identification = models.CharField(
        max_length = 30,
        blank = True,
        null = True,
        verbose_name = 'Identification',
        )


    identificationType = models.ForeignKey(
        'IdentificationType',
        blank = True,
        null = True,
        verbose_name = 'Identification Type',
        on_delete=models.CASCADE
        )

    profileImage = models.ImageField(
        upload_to='icon/profileimage/',
        max_length=255,
        null = True,
        blank = True,
        help_text='110px x 110 px',
        verbose_name = 'Profile Image'
        )

    referralCode = models.CharField(
        max_length = 100,
        blank = True,
        null = True,
        verbose_name = 'Referral Code',
        )

    gender = models.ForeignKey(
        'Gender',
        blank = True,
        null = True,
        verbose_name = 'Gender',
        on_delete=models.CASCADE
        )

    tag = models.TextField(
        verbose_name=u'Tag',
        blank=True,
        null=True,
        )

    freeShipping = models.BooleanField(
        verbose_name=u'Free Shipping',
        default=False
    )

    creditsAvailable = models.IntegerField(
        verbose_name = u'Credits Available',
        blank=True,
        null=True,
        default=0,
    )

    
    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = u'User'
        verbose_name_plural = u'Users'

class Address(models.Model):

    user = models.ForeignKey(
        'User',
        verbose_name=u'User',
        on_delete = models.CASCADE,
    )

    city = models.ForeignKey(
        'City',
        verbose_name = u'City',
        blank = True,
        on_delete = models.CASCADE,
    )

    address = models.CharField(
        max_length=255,
        verbose_name=u'Address',
    )

    buildingName = models.CharField(
        max_length=100,
        blank = True,
        null = True,
        verbose_name=u'Building',
    )

    unityNumber = models.CharField(
        max_length=100,
        blank = True,
        null = True,
        verbose_name=u'Number',
    )

    neighborhood = models.CharField(
        max_length=100,
        blank = True,
        null = True,
        verbose_name=u'Neighborhood',
    )

    addressType = models.ForeignKey(
        'AddressType',
        verbose_name=u'Address Type',
        on_delete = models.CASCADE,
    )

    latitude = models.DecimalField(
        max_digits=15, 
        decimal_places=10,
        blank=True,
        null=True,
        verbose_name=u'Latitude',
        )
    
    longitude = models.DecimalField(
        max_digits=15, 
        decimal_places=10,
        blank=True,
        null=True,
        verbose_name=u'Longitude',
        )

    tag = models.TextField(
        verbose_name=u'Tag',
        blank=True,
        null=True,
        )

    def __str__(self):
        return self.address + ' - ' +  self.city.name

    def getDirectionComplete(self):
        complete = self.address
        if not self.building_name == None:
            complete = complete + ' ' + self.building_name
        if not self.unity_number == None:
            complete = complete + ' ' + self.unity_number
        if not self.neighborhood == None:
            complete = complete + ' ' + self.neighborhood
        return complete

    class Meta:
        verbose_name = u'Address'
        verbose_name_plural = u'Addresses'

class IdentificationType(models.Model):
    """Models to set the id types allowed
    """
    name = models.CharField(
        max_length = 50,
        verbose_name = 'Name',
        )

    description = models.TextField(
        blank=True,
        null=True,
        verbose_name=u'Description',
        )

    icon = models.ImageField(
        upload_to='icon/IdentificationTypeIcon/',
        max_length=255,
        null = True,
        blank = True,
        help_text='110 x 110',
        verbose_name=u'Icon',
        )
    
    tag = models.TextField(
        verbose_name=u'Tag',
        blank=True,
        null=True,
        )

    def __str__(self):
        return self.name
    class Meta:
        verbose_name = u'Identification Type'
        verbose_name_plural = u'Identification Types'

class Gender(models.Model):
    """Models to set the gender types allowed
    """
    name = models.CharField(
        max_length = 50,
        verbose_name = 'Name',
        )

    description = models.TextField(
        blank=True,
        null=True,
        verbose_name=u'Description',
        )

    icon = models.ImageField(
        upload_to='icon/GenderIcon/',
        max_length=255,
        null = True,
        blank = True,
        help_text='110 x 110',
        verbose_name=u'Icon',
        )

    tag = models.TextField(
        verbose_name=u'Tag',
        blank=True,
        null=True,
        )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = u'Gender'
        verbose_name_plural = u'Genders'

class Country(models.Model):
    """Models that contains all cities
    """


    name = models.CharField(
        max_length = 50,
        verbose_name = 'Name',
        )

    description = models.TextField(
        blank=True,
        null=True,
        verbose_name=u'Description',
        )
    
    countryCode = models.IntegerField(
        verbose_name = u'Country Code',
        blank=True,
        null=True,
    )

    moneyCode = models.CharField(
        max_length=3,
        verbose_name = u'Money Code',
        blank=True,
        null=True,
    )
    
    icon = models.ImageField(
        upload_to='icon/CountriesIcon/',
        max_length=255,
        null = True,
        blank = True,
        help_text='110 X 110',
        verbose_name='Icon'
        )

    tag = models.TextField(
        verbose_name=u'Tag',
        blank=True,
        null=True,
        )
    

    def __str__(self):
        return (self.name)

    class Meta:
        verbose_name = u'Country'
        verbose_name_plural = u'Countries'

class City(models.Model):
    """Models that contains all cities
    """


    name = models.CharField(
        max_length = 50,
        verbose_name = 'Name',
        )

    description = models.TextField(
        blank=True,
        null=True,
        verbose_name=u'Description',
        )

    country = models.ForeignKey(
        'Country',
        blank = True,
        null = True,
        verbose_name = 'Country',
        on_delete=models.CASCADE
        )

    latitude = models.DecimalField(
        max_digits=9, 
        decimal_places=6,
        blank=True,
        null=True,
        verbose_name=u'Latitude',
        )
    
    longitude = models.DecimalField(
        max_digits=9, 
        decimal_places=6,
        blank=True,
        null=True,
        verbose_name=u'Longitude',
        )

    icon = models.ImageField(
        upload_to='icon/CityIcon/',
        max_length=255,
        null = True,
        blank = True,
        help_text='110 x 110',
        verbose_name='Icon'
        )

    tag = models.TextField(
        verbose_name=u'Tag',
        blank=True,
        null=True,
        )


    def __str__(self):
        return (self.name)

    class Meta:
        verbose_name = u'City'
        verbose_name_plural = u'Cities'

class AddressType(models.Model):
    """Models to set the address types allowed
    """
    name = models.CharField(
        max_length = 50,
        verbose_name = 'Name',
        )

    description = models.TextField(
        blank=True,
        null=True,
        verbose_name=u'Description',
        )

    icon = models.ImageField(
        upload_to='icon/AddressTypeIcon/',
        max_length=255,
        null = True,
        blank = True,
        help_text='110 x 110',
        verbose_name=u'Icon',
        )

    tag = models.TextField(
        verbose_name=u'Tag',
        blank=True,
        null=True,
        )

    def __str__(self):
        return self.name
    class Meta:
        verbose_name = u'Address Type'
        verbose_name_plural = u'Addesses Types'

class CodeType(models.Model):
    """Models to set the address types allowed
    """
    name = models.CharField(
        max_length = 50,
        verbose_name = 'Name',
        )

    description = models.TextField(
        blank=True,
        null=True,
        verbose_name=u'Description',
        )

    icon = models.ImageField(
        upload_to='icon/AddressTypeIcon/',
        max_length=255,
        null = True,
        blank = True,
        help_text='110 x 110',
        verbose_name=u'Icon',
        )

    tag = models.TextField(
        verbose_name=u'Tag',
        blank=True,
        null=True,
        )

    def __str__(self):
        return self.name
    class Meta:
        verbose_name = u'Code Type'
        verbose_name_plural = u'Code Types'

class UserCodes(models.Model):

    user = models.ForeignKey(
        'User',
        verbose_name=u'User',
        on_delete = models.CASCADE,
    )

    code = models.IntegerField(
        verbose_name=u'Code',
    )


    codeType = models.ForeignKey(
        'CodeType',
        verbose_name=u'Code Type',
        on_delete = models.CASCADE,
    )


    tag = models.TextField(
        verbose_name=u'Tag',
        blank=True,
        null=True,
        )

    expiresIn = models.DateTimeField(
        verbose_name = u'Expires In',
        auto_now=False, 
        auto_now_add=False,
    )

    def __str__(self):
        return self.user.phone + ' - ' +  str(self.code) + ' - ' + self.codeType.name


    class Meta:
        verbose_name = u'User Code'
        verbose_name_plural = u'User Codes'

class UserReferred(models.Model):

    userReferred = models.ForeignKey(
        'User',
        verbose_name=u'User Referred',
        related_name=u'UserReferred',
        on_delete = models.CASCADE,
    )

    userReferrer = models.ForeignKey(
        'User',
        verbose_name=u'User Referrer',
        related_name=u'UserReferrer',
        on_delete = models.CASCADE,
    )


    dateRegistered = models.DateTimeField(
        verbose_name = u'Date Registered',
        auto_now_add=True,
    )

    status = models.BooleanField(
        verbose_name=u'Status',
        default=False
    )

    def __str__(self):
        return self.userReferred.phone + ' - ' +  self.userReferrer.phone  + ' - ' + str(self.dateRegistered)


    class Meta:
        verbose_name = u'User Referral'
        verbose_name_plural = u'User Referrals'

class UserContacts(models.Model):

    user = models.ForeignKey(
        'User',
        verbose_name=u'User',
        related_name=u'User',
        on_delete = models.CASCADE,
    )

    comments = models.TextField(
        verbose_name = "Comments",
    )


    dateRegistered = models.DateTimeField(
        verbose_name = u'Date Registered',
        auto_now_add=True,
    )


    def __str__(self):
        return self.user.phone + ' - ' +  self.user.first_name  + ' - ' + str(self.dateRegistered)


    class Meta:
        verbose_name = u'User Comment'
        verbose_name_plural = u'User Comments'


class UserInteractions(models.Model):

    user = models.ForeignKey(
        'User',
        verbose_name=u'User',
        on_delete = models.CASCADE,
    )

    url = models.TextField(
        verbose_name=u'Url',
        blank=True,
        null=True,
        )

    viewName = models.CharField(
        max_length = 150,
        verbose_name = 'Name',
    )

    requestedData = models.TextField(
        verbose_name=u'Requested Data',
        blank=True,
        null=True,
    )

    resultData = models.TextField(
        verbose_name=u'Result Data',
        blank=True,
        null=True,
    )
    

    dateCreated = models.DateTimeField(
        verbose_name = u'Date Created',
        auto_now_add=True,
    )


    def __str__(self):
        return self.user.phone





















