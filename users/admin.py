from django.conf.urls import include
from django.contrib import admin
from .models import *
from .views import *
from django.urls import path
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm
from django import forms
from .forms import CustomUserCreationForm, CustomUserChangeForm



class UserCreateForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'first_name' , 'last_name', 'email', 'is_active', 'phone' )


class UserAdmin(UserAdmin):
    model: User
    list_filter = ('date_joined', 'is_active', 'groups')
    add_form = UserCreateForm
    form = CustomUserChangeForm
    prepopulated_fields = {'username': ('first_name' , 'last_name', )}
    list_display = ['username','first_name', 'last_name', 'is_staff', 'is_active', 'phone']
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('first_name', 'last_name',  'email', 'phone', 'is_active','username', 'password1', 'password2', ),
        }),
    )

    fieldsets = UserAdmin.fieldsets + (
            ('Extra Fields', {'fields': ('phone', 'identification', 'freeShipping', 'creditsAvailable')}),
    )

class UserInteractionsAdmin(admin.ModelAdmin):
    list_filter = ('viewName', 'dateCreated')


admin.site.register(User,UserAdmin)
admin.site.register(Address)
admin.site.register(IdentificationType)
admin.site.register(Gender)
admin.site.register(Country)
admin.site.register(City)
admin.site.register(AddressType)
admin.site.register(UserCodes)
admin.site.register(CodeType)
admin.site.register(UserReferred)
admin.site.register(UserContacts)
admin.site.register(UserInteractions, UserInteractionsAdmin)


