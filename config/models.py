from django.db import models



class DistancePrices(models.Model):
    """Models to set the address types allowed
    """

    name = models.CharField(
        max_length=255,
        verbose_name=u'Nombre',
    )

    description = models.TextField(
        blank=True,
        null=True,
        verbose_name=u'Description',
        )

    latitude = models.DecimalField(
        max_digits=19, 
        decimal_places=16,
        verbose_name=u'Latitude',
        )
        
    
    longitude = models.DecimalField(
        max_digits=19, 
        decimal_places=16,
        verbose_name=u'Longitude',
        )


    distance = models.DecimalField(
        max_digits=7, 
        decimal_places=3,
        verbose_name=u'Max Distance',
        )

    price = models.BigIntegerField(
        verbose_name =u'Ship Price',
        default=3000
    )


    def __str__(self):
        return self.name
    class Meta:
        verbose_name = u'Distance Price'
        verbose_name_plural = u'Distance Prices'