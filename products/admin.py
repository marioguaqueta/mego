from django.contrib import admin
from .models import *
from users.models import *
from django.urls import path
from products.views import *
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.forms import ModelForm, ModelMultipleChoiceField
from .forms import *




class ChoiceInLineStoreProduct(admin.TabularInline):
    model = Product



class ChoiceInLineProductAddition(admin.TabularInline):
    model = ProductAddition
    search_fields = ['name']
    form = ProductAdditionForm
    


class ChoiceInLineProductImages(admin.TabularInline):
    model = ProductImages
    
    
class ProductAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name', 'store__name']
    inlines = [ChoiceInLineProductAddition, ChoiceInLineProductImages]


class StoreAdminForm(ModelForm):
    class Meta:
        model = Store
        fields = '__all__'

    productCategory = ModelMultipleChoiceField(
        queryset=ProductCategory.objects.all(),
        required=True,
        widget=FilteredSelectMultiple(
            verbose_name='Product Category',
            is_stacked=False
        )
    )

    storeMultiCategory = ModelMultipleChoiceField(
        queryset=StoreCategory.objects.all(),
        required=True,
        widget=FilteredSelectMultiple(
            verbose_name='Store Multi Category',
            is_stacked=False
        )
    )


    city = ModelMultipleChoiceField(
        queryset=City.objects.all(),
        required=True,
        widget=FilteredSelectMultiple(
            verbose_name='City',
            is_stacked=False
        )
    )


class StoreAdmin(admin.ModelAdmin):
    list_display = ['name', 'address', 'is_active']
    list_filter = ['priority', 'is_active',]
    search_fields = ['name']
    filter_horizontal = ('oppeningTimes',)
    inlines = [ChoiceInLineStoreProduct,]
    form = StoreAdminForm


class ChoiceInLineProductOrder(admin.TabularInline):
    model = ProductOrder

class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'store', 'clientUser', 'address', 'paymentMethod', 'state']
    inlines = [ChoiceInLineProductOrder,]


class OrdersViewModel(models.Model):

    class Meta:
        verbose_name_plural = 'Review Orders'
        app_label = 'products'

class OrdersViewModelAdmin(admin.ModelAdmin):
    model = OrdersViewModel

    def get_urls(self):
        view_name = '{}_{}_changelist'.format(
            self.model._meta.app_label, self.model._meta.model_name)
        return [
            path('mP5vCd-admin/', ReviewOrders.as_view(), name=view_name),
        ]


admin.site.register(OrdersViewModel, OrdersViewModelAdmin)



# Order Specific
class OrderViewModel(models.Model):

    class Meta:
        verbose_name_plural = 'Singular Order'
        app_label = 'products'

class OrderViewModelAdmin(admin.ModelAdmin):
    model = OrderViewModel

    def get_urls(self):
        view_name = '{}_{}_changelist'.format(
            self.model._meta.app_label, self.model._meta.model_name)
        return [
            path('mP5vCd-admin/', ReviewOrder.as_view(), name=view_name),
        ]

admin.site.register(OrderViewModel, OrderViewModelAdmin)



# Order Report
class OrderReportModel(models.Model):

    class Meta:
        verbose_name_plural = 'Order Report'
        app_label = 'products'

class OrderReportModelAdmin(admin.ModelAdmin):
    model = OrderViewModel

    def get_urls(self):
        view_name = '{}_{}_changelist'.format(
            self.model._meta.app_label, self.model._meta.model_name)
        return [
            path('mP5vCd-admin/', OrderReport.as_view(), name=view_name),
        ]

admin.site.register(OrderReportModel, OrderReportModelAdmin)


# Order Total Report
class OrderTotalReportModel(models.Model):

    class Meta:
        verbose_name_plural = 'Order Total Report'
        app_label = 'products'

class OrderTotalReportModelAdmin(admin.ModelAdmin):
    model = OrderTotalReportModel

    def get_urls(self):
        view_name = '{}_{}_changelist'.format(
            self.model._meta.app_label, self.model._meta.model_name)
        return [
            path('mP5vCd-admin/', OrderTotalReport.as_view(), name=view_name),
        ]

admin.site.register(OrderTotalReportModel, OrderTotalReportModelAdmin)


admin.site.register(StoreSection)
admin.site.register(StoreCategory)
admin.site.register(Store, StoreAdmin)
admin.site.register(DeliveryType)
admin.site.register(ProductCategory)
admin.site.register(Product,ProductAdmin)
admin.site.register(ProductAddition)
admin.site.register(AdditionOrder)
admin.site.register(ProductOrder)
admin.site.register(Addition)
admin.site.register(AdditionType)
admin.site.register(Taxes)
admin.site.register(Promotion)
admin.site.register(Order, OrderAdmin)
admin.site.register(PaymentMethod)
admin.site.register(OrderState)
admin.site.register(OpenningTimes)
admin.site.register(Banner)


