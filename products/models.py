from django.db import models
from django.contrib.auth.models import AbstractUser, AbstractBaseUser, Group
from users.models import *
from mego.local_settings import *



class StoreSection(models.Model):
    """Models to set the store section allowed
    """
    name = models.CharField(
        max_length = 50,
        verbose_name = 'Name',
        )

    description = models.TextField(
        blank=True,
        null=True,
        verbose_name=u'Description',
        )

    tag = models.TextField(
        verbose_name=u'Tag',
        blank=True,
        null=True,
        )

    priority = models.IntegerField(
        verbose_name=u'Priority',
        )

    status = models.BooleanField(
        verbose_name=u'Status',
    )

    icon = models.FileField(
        upload_to='icon/StoreSectionIcon/',
        max_length=255,
        help_text='110 x 110',
        verbose_name=u'Icon',
        )

    def __str__(self):
        return self.name
    class Meta:
        verbose_name = u'Store Section'
        verbose_name_plural = u'Store Sections'

class StoreCategory(models.Model):
    """Models to set the gender types allowed
    """
    name = models.CharField(
        max_length = 50,
        verbose_name = 'Name',
        )

    description = models.TextField(
        blank=True,
        null=True,
        verbose_name=u'Description',
        )

    priority = models.IntegerField(
        verbose_name=u'Priority',
        )

    tag = models.TextField(
        verbose_name=u'Tag',
        blank=True,
        null=True,
        )

    section = models.ForeignKey(
        'StoreSection',
        verbose_name=u'Store Section',
        on_delete = models.CASCADE,
    )

    status = models.BooleanField(
        verbose_name=u'Status',
    )

    icon = models.FileField(
        upload_to='icon/StoreCategory/',
        max_length=255,
        help_text='110 x 110',
        verbose_name=u'Icon',
        )

    color = models.CharField(
        max_length = 6,
        verbose_name = 'Color',
        )
    
    noDeliveryPrice = models.BooleanField(
        verbose_name=u'No delivery price',
        default = False
    )
    

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = u'Store Category'
        verbose_name_plural = u'Store Categories'

class OpenningTimes(models.Model):

    weekday = models.IntegerField(
        choices=WEEKDAYS,
        verbose_name = 'Día',
    )
    from_hour = models.TimeField(
        verbose_name = 'Hora Inicio',
    )
    to_hour = models.TimeField(
        verbose_name = 'Hora Fin',
    )

    def __str__(self):
        return str(WEEKDAYS_DICT[self.weekday]) + ' - ' + str(self.from_hour.strftime("%H:%M")) + ' A ' + str(self.to_hour.strftime("%H:%M"))

    class Meta:
        verbose_name = u'Hora de apertura'
        verbose_name_plural = u'Horas de apertura'

class Store(models.Model):
    """Models that contains all cities
    """
    name = models.CharField(
        max_length = 50,
        verbose_name = 'Name',
        )

    description = models.TextField(
        blank=True,
        null=True,
        verbose_name=u'Description',
        )
    
    tag = models.TextField(
        verbose_name=u'Tag',
        blank=True,
        null=True,
        )

    priority = models.IntegerField(
        verbose_name = u'Priority',
    )

    percentage = models.IntegerField(
        verbose_name = u'Percentage',
    )

    meanTime = models.IntegerField(
        verbose_name = u'Mean Time',
    )

    oppeningTimes = models.ManyToManyField(
        OpenningTimes,
        verbose_name = u'Horas de apertura',
        blank= True,
        )

    startHour = models.TimeField(
        verbose_name = u'Start Hour',
        )

    finishHour = models.TimeField(
        verbose_name = u'Finish Hour',
        )

    is_active = models.BooleanField(
        verbose_name=u'Status',
        default = True,
    )

    free_shipping = models.BooleanField(
        verbose_name=u'Free Shipping',
        default = False,
    )

    latitude = models.DecimalField(
        max_digits=9, 
        decimal_places=6,
        verbose_name=u'Latitude',
        )
    
    longitude = models.DecimalField(
        max_digits=9, 
        decimal_places=6,
        verbose_name=u'Longitude',
        )

    igUrl = models.URLField(
        max_length=200, 
        verbose_name=u'Instagram Url',
        blank=True,
        null=True,
        )

    fbUrl = models.URLField(
        max_length=200, 
        verbose_name=u'Facebook Url',
        blank=True,
        null=True,
        )

    address = models.CharField(
        max_length = 50,
        verbose_name = 'Address',
    )

    phone = models.CharField(
        max_length = 50,
        blank = True,
        null=True,
        verbose_name = 'Phone',
    )

    deliveryType = models.ForeignKey(
        'DeliveryType', 
        verbose_name=u'Delivery Type',
        on_delete=models.CASCADE
        )

    productCategory = models.ManyToManyField(
        'ProductCategory',
        verbose_name=u'Product Category'
        )
    
    storeMultiCategory = models.ManyToManyField(
        'StoreCategory',
        verbose_name=u'Store MultiCategory',
        related_name = "storeMultiCategory",
        )

    storeCategory = models.ForeignKey(
        'StoreCategory',
        verbose_name=u'Store Category',
        on_delete=models.CASCADE,
        related_name = "storeCategory",
        null=True,
        )
    
    icon = models.ImageField(
        upload_to='icon/StoreIcon/',
        max_length=255,
        help_text='110 X 110',
        verbose_name='Icon'
        )

    city = models.ManyToManyField(
        'users.City',
        verbose_name = u'City',
        blank = True,
    )




    def __str__(self):
        return (self.name)

    class Meta:
        verbose_name = u'Store'
        verbose_name_plural = u'Stores'

class DeliveryType(models.Model):
    """Models that contains all delivery types
    """

    name = models.CharField(
        max_length = 50,
        verbose_name = 'Name',
        )

    description = models.TextField(
        blank=True,
        null=True,
        verbose_name=u'Description',
        )

    userGroup = models.ManyToManyField(
        'auth.Group',
        verbose_name=u'Group',
        )
        

    icon = models.ImageField(
        upload_to='icon/CityIcon/',
        max_length=255,
        help_text='110 x 110',
        verbose_name='Icon'
        )

    def __str__(self):
        return (self.name)

class ProductCategory(models.Model):

    name = models.CharField(
        max_length=100,
        verbose_name = u'Name',
    )

    description = models.CharField(
        max_length=100,
        verbose_name = u'Description',
    )

    icon = models.ImageField(
        upload_to='icon/ProductCategoryIcon/',
        max_length=255,
        help_text='110 x 110',
        verbose_name='Icon'
    )

    is_active = models.BooleanField(
        default = True,
        verbose_name = u'Active',
    )
    
    priority = models.IntegerField(
        verbose_name = u'Priority',
    )

    StoreCategory = models.ForeignKey(
        'StoreCategory',
        verbose_name=u'Store Category',
        on_delete = models.CASCADE,
        )

    color = models.CharField(
        max_length = 6,
        verbose_name = 'Color',
        default= 'E9E9E9'
        )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = u'Product Category'
        verbose_name_plural = u'Product Categories'

class Product(models.Model):

    name = models.CharField(
        max_length=100,
        verbose_name = u'Name',
    )

    productCategory = models.ForeignKey(
        'ProductCategory',
        verbose_name =  u'Category',
        on_delete = models.CASCADE,
    )

    store = models.ForeignKey(
        'Store',
        verbose_name =  u'Store',
        on_delete = models.CASCADE,
    )

    price = models.BigIntegerField(
        verbose_name = u'Price',
    )

    percentageDiscount = models.BigIntegerField(
        verbose_name = u'Percentage Discount',
    )


    qualify = models.IntegerField(
        verbose_name = u'Qualify',
        default = 5
    )

    tax = models.ForeignKey(
        'Taxes',
        verbose_name = u'Tax',
        on_delete = models.CASCADE,
    )

    priority = models.IntegerField(
        verbose_name = u'Priority',
    )


    is_active = models.BooleanField(
        default = True,
        verbose_name = u'Status',
    )


    icon = models.ImageField(
        verbose_name=u'Icon',
        upload_to='icon/ProductIcon/',
        max_length=255,
        help_text='300x300',
    )
    
    shortDescription =  models.CharField(
        max_length=100,
        verbose_name = u'Short Description',
    )
    description = models.TextField(
        blank = True,
        null = True,
        verbose_name=u'Description',
    )	
	
    def __str__(self):
        return self.name + ' - ' + self.store.name


    class Meta:
        verbose_name = u'Product'
        verbose_name_plural = u'Products'

class ProductAddition(models.Model):


    product = models.ForeignKey(
        'Product',
        verbose_name =  u'Product',
        blank = True,
        null = True,
        on_delete = models.CASCADE,
    )

    addition = models.ForeignKey(
        'Addition',
        verbose_name =  u'Addition',
        blank = True,
        null = True,
        on_delete = models.CASCADE,
    )

    description = models.TextField(
        blank = True,
        null = True,
        verbose_name=u'Description',
    )

    is_active = models.BooleanField(
        default = True,
        verbose_name = u'Status',
    )

    price = models.BigIntegerField(
        verbose_name = u'Price',
        default="0",
    )
    
    class Meta:
        verbose_name = u'Product Addition'
        verbose_name_plural = u'Products Addition'

	
    def __str__(self):
        if self.product is not None and self.addition is not None:
            return self.product.name + ' - ' + self.addition.name
        elif self.product is None:
            return  'No Product - ' + self.addition.name
        elif self.addition is None:
            return self.product.name + ' - No Addition'
        else:
            return 'For Delete'

class ProductImages(models.Model):


    product = models.ForeignKey(
        'Product',
        verbose_name =  u'Product',
        blank = True,
        null = True,
        on_delete = models.CASCADE,
    )

    isActive = models.BooleanField(
        default = True,
        verbose_name = u'Status',
    )

    icon = models.ImageField(
        verbose_name=u'Icon',
        upload_to='icon/ProductImageIcon/',
        max_length=255,
        help_text='300x300',
    )

    
    class Meta:
        verbose_name = u'Product Image'
        verbose_name_plural = u'Products Images'

	
    def __str__(self):
        return self.product.name

class Addition(models.Model):

    name = models.CharField(
        max_length=100,
        verbose_name = u'Name',
    )

    shortDescription = models.TextField(
        blank = True,
        null = True,
        verbose_name=u'Short Description',
    )

    priority = models.IntegerField(
        verbose_name = u'Priority',
        default=1,
    )


    is_active = models.BooleanField(
        default = True,
        verbose_name = u'Active',
    )


    icon = models.ImageField(
        verbose_name=u'Icon',
        upload_to='icon/ProductAdditionIcon/',
        max_length=255,
        help_text='300x300',
        blank = True,
        null = True,
    )

    additionType = models.ForeignKey(
        'AdditionType',
        verbose_name =  u'Addition Type',
        on_delete = models.CASCADE,
    )
	
    def __str__(self):
        if self.additionType.onlyOne:
            return self.name + ' - ' + self.additionType.name  + ' - Unico'
        else:
            return self.name + ' - ' + self.additionType.name  + ' - Multiple'


    class Meta:
        verbose_name = u'Addition'
        verbose_name_plural = u'Additions'

class AdditionType(models.Model):

    name = models.CharField(
        max_length=100,
        verbose_name = u'Name',
    )

    shortDescription = models.TextField(
        blank = True,
        null = True,
        verbose_name=u'Short Description',
    )

    priority = models.IntegerField(
        verbose_name = u'Priority',
    )


    is_active = models.BooleanField(
        default = True,
        verbose_name = u'Active',
    )

    onlyOne = models.BooleanField(
        default = True,
        verbose_name = u'Only One',
    )

    icon = models.ImageField(
        verbose_name=u'Icon',
        upload_to='icon/ProductAdditionIcon/',
        max_length=255,
        help_text='300x300',
        blank = True,
        null = True,
    )

    
	
    def __str__(self):
        return self.name + ' - ' + str(self.onlyOne)


    class Meta:
        verbose_name = u'Product Addition Type'
        verbose_name_plural = u'Products Addition Types'

class Taxes(models.Model):

    name = models.CharField(
        max_length=100,
        verbose_name=u'Name',
    )

    percent = models.FloatField(
        verbose_name = u'Percent',
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = u'Tax'
        verbose_name_plural = u'Taxes'

class Promotion(models.Model):

    name = models.CharField(
        max_length=255, 
        verbose_name="Name"
        )

    shortDescription = models.CharField(
        max_length=255, 
        verbose_name="Short Description"
        )


    banner = models.ImageField(
        max_length=100,
        verbose_name=u'Banner',
    )

    product = models.ForeignKey(
        'Product', 
        verbose_name='Product', 
        on_delete=models.CASCADE
        )

    status = models.BooleanField(
        verbose_name= u'Status',
        default= True
    )

    expiresIn = models.DateTimeField(
        verbose_name = u'Expires In',
        auto_now=False, 
        auto_now_add=False,
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = u'Promotion'
        verbose_name_plural = u'Promotion'

class Order(models.Model):

    store = models.ForeignKey(
        'Store',
        verbose_name = u'Store',
        on_delete = models.CASCADE,
        related_name=u'Store',
    )

    clientUser = models.ForeignKey(
        'users.User',
        verbose_name = u'Client User',
        on_delete = models.CASCADE,
        related_name=u'ClientUser',
    )

    deliveredBy = models.ForeignKey(
        'users.User',
        verbose_name = u'Delivery User',
        on_delete = models.CASCADE,
        related_name=u'DeliveredBy',
        blank=True,
        null=True,
    )

    orderDate = models.DateTimeField(
        verbose_name = u'Date Ordered',
        auto_now_add=True,
    )

    address = models.ForeignKey(
        'users.Address',
        verbose_name = 'Address',
        on_delete = models.CASCADE,
    )

    paymentMethod = models.ForeignKey(
        'PaymentMethod',
        verbose_name = u'Payment Method',
        on_delete = models.CASCADE,
    )

    state = models.ForeignKey(
        'OrderState',
        verbose_name =u'State',
        on_delete = models.CASCADE,
    )


    shipPrice = models.BigIntegerField(
        verbose_name =u'Ship Price',
        blank = True,
        null = True,
    )

    observation = models.TextField(
        blank = True,
        null = True,
        verbose_name = u'Order observations',
    )

    def __str__(self):
        return self.clientUser.phone + ' - ' + self.address.address

    def total(self):
        total = 0
        products = ProductOrder.objects.filter(order__id = self.id)
        for product in products:
            total += (product.quantity * product.viewedPrice)
            additions = AdditionOrder.objects.filter(productOrder__id = product.id)
            for addition in additions:
                total += (addition.quantity * addition.viewedPrice)
        return total
    
    def totalDelivery(self):
        total = 0
        products = ProductOrder.objects.filter(order__id = self.id)
        for product in products:
            total += (product.quantity * product.viewedPrice)
            additions = AdditionOrder.objects.filter(productOrder__id = product.id)
            for addition in additions:
                total += (addition.quantity * addition.viewedPrice)
        total += self.shipPrice
        return total


    class Meta:
        verbose_name = u'Order'
        verbose_name_plural = u'Orders'

class PaymentMethod(models.Model):

    name = models.CharField(
        max_length=100,
        verbose_name=u'Name',
    )

    icon = models.ImageField(
        verbose_name=u'Icon',
        upload_to='icon/PaymentMethodIcon/',
        max_length=255,
        help_text='300x300',
        blank = True,
        null = True,
    )


    def __str__(self):
        return self.name

    class Meta:
        verbose_name = u'Payment Method'
        verbose_name_plural = u'Payment Method'

class OrderState(models.Model):

    name = models.CharField(
        max_length=100,
        verbose_name=u'Name',
    )

    description = models.TextField(
        blank=True,
        null=True,
        verbose_name=u'Description',
        )

    icon = models.ImageField(
        upload_to='icon/OrderStateIcon/',
        max_length=255,
        help_text='110 x 110',
        verbose_name='Icon',
        blank=True,
        null=True,
        )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = u'Order State'
        verbose_name_plural = u'Order states'

class ProductOrder(models.Model):

    order = models.ForeignKey(
        'Order',
        verbose_name = u'Order',
        on_delete = models.CASCADE,
    )

    product = models.ForeignKey(
        'Product',
        verbose_name = u'Product',
        on_delete = models.CASCADE,
    )

    quantity = models.SmallIntegerField(
        default = 1,
        verbose_name = u'Quantity',
    )

    viewedPrice = models.BigIntegerField(
        verbose_name = u'Price',
    )
    	
    observation = models.TextField(
        blank = True,
        null = True,
        verbose_name = u'Observations',
    )

    orderDate = models.DateTimeField(
        verbose_name = u'Date Ordered',
        auto_now_add=True,
    )

    def __str__(self):
        return self.product.name


    class Meta:
        verbose_name = u'Order Product'
        verbose_name_plural = u'Orders Products'

class AdditionOrder(models.Model):

    productOrder = models.ForeignKey(
        'ProductOrder',
        verbose_name = u'Product Order',
        on_delete = models.CASCADE,
    )

    addition = models.ForeignKey(
        'ProductAddition',
        verbose_name = u'Addition',
        on_delete = models.CASCADE,
    )

    quantity = models.SmallIntegerField(
        default = 1,
        verbose_name = u'Quantity',
    )

    viewedPrice = models.BigIntegerField(
        verbose_name = u'Price',
    )

    orderDate = models.DateTimeField(
        verbose_name = u'Date Ordered',
        auto_now_add=True,
    )
    	
    observation = models.CharField(
        blank = True,
        null = True,
		max_length=100,
        verbose_name = u'Observations',
    )
    

    def __str__(self):
        return self.addition.product.name

    class Meta:
        verbose_name = u'Order Product Addition'
        verbose_name_plural = u'Order Product Additions'

class Banner(models.Model):

    name = models.CharField(
        max_length=254,
        verbose_name=u'Name',
    )

    status = models.BooleanField(
        verbose_name=u'Status',
        default = True,
    )

    icon = models.ImageField(
        upload_to='icon/BannerImages/',
        max_length=255,
        help_text='300 x 150',
        verbose_name='Icon',
        blank=True,
        null=True,
        )

    product = models.ForeignKey(
        'Product', 
        verbose_name='Product', 
        on_delete=models.CASCADE,
        blank= True,
        null= True,
        )


    expiresIn = models.DateTimeField(
        verbose_name = u'Expires In',
        auto_now=False, 
        auto_now_add=False,
        blank= True,
        null= True,
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = u'Banner'
        verbose_name_plural = u'Banners'



