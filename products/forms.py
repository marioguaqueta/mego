from dal import autocomplete

from django import forms
from .models import *
from .views import *
class ProductAdditionForm(forms.ModelForm):
    class Meta:
        model = ProductAddition
        fields = ('__all__')
        widgets = {
            'addition': autocomplete.ModelSelect2(url='product-addition-autocomplete')
        }
