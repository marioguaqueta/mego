import json
import datetime  
import zipfile
import unidecode


from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import View
from django.shortcuts import render, redirect, get_object_or_404, HttpResponseRedirect
from products.models import *
from itertools import chain
from datetime import datetime, timedelta
from dal import autocomplete



import xlsxwriter

import io


class ReviewOrders(View):
    
    def get(self, request):
        today = datetime.today()  - timedelta(days = 10)
        orders = Order.objects.filter(orderDate__gt = today).order_by('-orderDate')
        productOrders = ProductOrder.objects.filter(orderDate__gt = today)
        productAddition = AdditionOrder.objects.filter(orderDate__gt = today)
        return render(request, 'review_orders.html', {'orders':orders, 'productOrders':productOrders, 'productAddition':productAddition})


class ReviewOrder(View):
    
    def get(self, request):
        orders = Order.objects.filter(id = request.GET['oid']).order_by('-orderDate')
        productOrders = ProductOrder.objects.filter(order__in = orders)
        productAddition = AdditionOrder.objects.filter(productOrder__in = productOrders)
        return render(request, 'review_order.html', {'order':orders[0], 'productOrders':productOrders, 'productAddition':productAddition})

class OrderReport(View):
    
    def get(self, request):
        fecha_inicio = request.GET.get('ini')
        fecha_fin = request.GET.get('fin')
        if fecha_inicio is not None and fecha_fin is not None:
            fecha_inicio = datetime.strptime(request.GET.get('ini'), '%Y-%m-%d')
            fecha_fin = datetime.strptime(request.GET.get('fin'), '%Y-%m-%d')
            orders = Order.objects.filter(state__name = 'Aceptada', orderDate__lte=fecha_fin, orderDate__gte=fecha_inicio).order_by('-orderDate', 'store__name')
        else:
            orders = Order.objects.filter(state__name = 'Aceptada').order_by('-orderDate', 'store__name')

        curdate = datetime.now().strftime('%Y-%m-%d %H:%M')
        
        stores = []
        file_names = []
        for order in orders:
            if order.store.name not in stores:
                stores.append(unidecode.unidecode(order.store.name)) 
        
        for store in stores:     
            file_name = unidecode.unidecode(store.upper()) + ' - '+ curdate + '.xlsx'
            file_path_save = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+ "/media/reports/" + file_name
            file_names.append(file_path_save)
            workbook = xlsxwriter.Workbook(file_path_save)
            bold = workbook.add_format({'bold': True})
            money = workbook.add_format({'num_format': '$#,##'})
            cell_format = workbook.add_format()
            cell_format.set_text_wrap()
            worksheet = workbook.add_worksheet(store.upper())
            row = 1
            col = 0
            worksheet.write('A1', 'Número', bold)
            worksheet.write('B1', 'Aliado', bold)
            worksheet.write('C1', 'Fecha', bold)
            worksheet.write('D1', 'Metodo Pago', bold)
            worksheet.write('E1', 'Estado', bold)
            worksheet.write('F1', 'Envio', bold)
            worksheet.write('G1', 'Productos', bold)
            worksheet.write('H1', 'Total', bold)
            worksheet.write('I1', 'Comision', bold)
            worksheet.write('J1', 'Productos', bold)

            for order in orders:
                if order.store.name == store:
                    col = 0
                    worksheet.write(row, col, order.id)
                    col += 1
                    worksheet.write(row, col, order.store.name)
                    col += 1
                    worksheet.write(row, col, order.orderDate.strftime('%Y-%m-%d %H:%M'))
                    col += 1
                    worksheet.write(row, col, order.paymentMethod.name)
                    col += 1
                    worksheet.write(row, col, order.state.name)
                    col += 1
                    worksheet.write(row, col, order.shipPrice, money)
                    col += 1
                    worksheet.write(row, col, order.total(), money)
                    col += 1
                    worksheet.write(row, col, order.totalDelivery(), money)
                    col += 1
                    worksheet.write(row, col, order.total() * 0.05, money)
                    col += 1
                    txt_prod = ''
                    products = ProductOrder.objects.filter(order = order)
                    for product in products:
                        txt_prod += str(product.product.name)
                        txt_prod += ' - '
                        txt_prod += str(product.quantity)
                        txt_prod += ' - $'
                        txt_prod += str(product.viewedPrice)
                        txt_prod += '\n'
                    worksheet.write(row, col, txt_prod, cell_format)
                    row += 1
            workbook.close()

        file_zip_path_save = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+ "/media/reports/reporte_" + curdate + '.zip'
        zipObj = zipfile.ZipFile(file_zip_path_save, 'w', zipfile.ZIP_DEFLATED)
        for name in file_names:
            zipObj.write(name)
        zipObj.close()

        response = HttpResponse(open(file_zip_path_save, 'rb'),content_type='application/zip')
        response['Content-Disposition'] = 'attachment; filename=Reportes.zip'
        return response



class OrderTotalReport(View):
    
    def get(self, request):
        fecha_inicio = request.GET.get('ini')
        fecha_fin = request.GET.get('fin')
        if fecha_inicio is not None and fecha_fin is not None:
            fecha_inicio = datetime.strptime(request.GET.get('ini'), '%Y-%m-%d')
            fecha_fin = datetime.strptime(request.GET.get('fin'), '%Y-%m-%d')
            orders = Order.objects.filter(state__name = 'Aceptada', orderDate__lte=fecha_fin, orderDate__gte=fecha_inicio).order_by('-orderDate', 'store__name')
        else:
            orders = Order.objects.filter(state__name = 'Aceptada').order_by('-orderDate', 'store__name')
        
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        worksheet = workbook.add_worksheet("Total Ventas")
                        
        bold = workbook.add_format({'bold': True})
        money = workbook.add_format({'num_format': '$#,##'})
        cell_format = workbook.add_format()
        cell_format.set_text_wrap()
            
        row = 1
        col = 0
        worksheet.write('A1', 'Número', bold)
        worksheet.write('B1', 'Aliado', bold)
        worksheet.write('C1', 'Fecha', bold)
        worksheet.write('D1', 'Metodo Pago', bold)
        worksheet.write('E1', 'Estado', bold)
        worksheet.write('F1', 'Envio', bold)
        worksheet.write('G1', 'Productos', bold)
        worksheet.write('H1', 'Total', bold)
        worksheet.write('I1', 'Comision', bold)
        worksheet.write('J1', 'Productos', bold)

        for order in orders:
            col = 0
            worksheet.write(row, col, order.id)
            col += 1
            worksheet.write(row, col, order.store.name)
            col += 1
            worksheet.write(row, col, order.orderDate.strftime('%Y-%m-%d %H:%M'))
            col += 1
            worksheet.write(row, col, order.paymentMethod.name)
            col += 1
            worksheet.write(row, col, order.state.name)
            col += 1
            worksheet.write(row, col, order.shipPrice, money)
            col += 1
            worksheet.write(row, col, order.total(), money)
            col += 1
            worksheet.write(row, col, order.totalDelivery(), money)
            col += 1
            worksheet.write(row, col, order.total() * 0.05, money)
            col += 1
            txt_prod = ''
            products = ProductOrder.objects.filter(order = order)
            for product in products:
                txt_prod += str(product.product.name)
                txt_prod += ' - '
                txt_prod += str(product.quantity)
                txt_prod += ' - $'
                txt_prod += str(product.viewedPrice)
                txt_prod += '\n'
            worksheet.write(row, col, txt_prod, cell_format)
            row += 1
        workbook.close()
        output.seek(0)

        response = HttpResponse(
            output,
            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        )
        response['Content-Disposition'] = 'attachment; filename=Reporte.xlsx'

        return response





class ProductAdittion(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated:
            return Addition.objects.none()

        qs = Addition.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs