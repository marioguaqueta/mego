from django.conf.urls import *
from .views import *

urlpatterns = [
    url(
        r'^product-addition-autocomplete/$',
        ProductAdittion.as_view(),
        name='product-addition-autocomplete',
    ),
]