from django.contrib import admin
from .models import *


class PUSHAdmin(admin.ModelAdmin):
    search_fields = ['name', 'title',]
    list_filter = ['inmediately', 'is_active',]


class SMSAdmin(admin.ModelAdmin):
    search_fields = ['name', 'message',]
    list_filter = ['inmediately', 'is_active',]




admin.site.register(PushMessage, PUSHAdmin)
admin.site.register(SMSMessage, SMSAdmin)
admin.site.register(CronLog)

