from django.apps import AppConfig


class MegoMessagesConfig(AppConfig):
    name = 'mego_messages'
