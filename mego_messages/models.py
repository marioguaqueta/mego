

import datetime

from django.db import models
from django.db.models import Q
from users.models import *
from products.models import *
from mego.local_settings import *
from api.util_functions import *


# FCM
from fcm_django.models import FCMDevice

class PushMessage(models.Model):
    """Models to set the store section allowed
    """
    name = models.CharField(
        max_length = 150,
        verbose_name = 'Name',
        )
    
    title = models.CharField(
        max_length = 50,
        verbose_name = 'Title',
        )

    message = models.TextField(
        verbose_name=u'Message',
        )

    segment = models.CharField(
        max_length=32,
        choices=MESSAGE_SEGMENTS,
        default='1',
    )


    inmediately = models.BooleanField(
        verbose_name=u'Inmediately',
        default = False,
    )

    initDate = models.DateField(
        verbose_name = u'Init Date',
        blank= True,
        null=True
    )

    finishDate = models.DateField(
        verbose_name = u'Finish Date',
        blank= True,
        null=True
    )

    sendHour = models.TimeField(
        verbose_name = u'Send Hour',
        blank= True,
        null=True
    )

    is_active = models.BooleanField(
        verbose_name=u'Status',
        default = True,
    )

    def save(self, *args, **kwargs):
        if(self.inmediately):
            users = None
            if(self.segment == '1'):
                users = User.objects.filter(groups__name='Client')
            devices = FCMDevice.objects.filter(user__in = users)
            devices.send_message(title= self.title, body= self.message)
        super(PushMessage, self).save(*args, **kwargs)


    def __str__(self):
        return self.name
    class Meta:
        verbose_name = u'Push Message'
        verbose_name_plural = u'Push Messages'


class SMSMessage(models.Model):
    """Models to set the store section allowed
    """
    name = models.CharField(
        max_length = 150,
        verbose_name = 'Name',
        )

    message = models.TextField(
        verbose_name=u'Message',
        )

    segment = models.CharField(
        max_length=32,
        choices=MESSAGE_SEGMENTS,
        default='1',
    )

    inmediately = models.BooleanField(
        verbose_name=u'Inmediately',
        default = False,
    )

    initDate = models.DateField(
        verbose_name = u'Init Date',
        blank= True,
        null=True
    )

    finishDate = models.DateField(
        verbose_name = u'Finish Date',
        blank= True,
        null=True
    )

    sendHour = models.TimeField(
        verbose_name = u'Send Hour',
        blank= True,
        null=True
    )

    is_active = models.BooleanField(
        verbose_name=u'Status',
        default = True,
    )

    def save(self, *args, **kwargs):
        if(self.inmediately):
            users = None
            if(self.segment == '1'):
                users = User.objects.filter(groups__name='Client')
            phones = []
            if len(users) > 0:
                for user in users:
                    phoneAppend = {"msisdn": "57" + str(user.phone)}
                    if phoneAppend not in phones :
                        phones.append(phoneAppend)
                sendMarketingSMS(phones,self.message)
        super(SMSMessage, self).save(*args, **kwargs)


    def __str__(self):
        return self.name
    class Meta:
        verbose_name = u'SMS Message'
        verbose_name_plural = u'SMS Messages'



class CronLog(models.Model):
    """Models to set the store section allowed
    """
    job = models.CharField(
        max_length = 150,
        verbose_name = 'Job Name',
        )

    executionTime = models.DateTimeField(
        verbose_name = u'Execution DateTime',
        blank= True,
        null=True
    )


    def __str__(self):
        return self.job
    class Meta:
        verbose_name = u'Cron Log'
        verbose_name_plural = u'Cron Logs'
