
import datetime

from api.util_functions import *
from domi_messages.models import *
from users.models import *
from products.models import *
from domiantojo.local_settings import *

# FCM
from fcm_django.models import FCMDevice



def push_job():
    current_time = datetime.now()
    log = CronLog(
        job = 'push_job',
        executionTime = current_time
    )
    log.save()
    push_messages = PushMessage.objects.filter(is_active = True, initDate__lte = current_time, finishDate__gte = current_time, sendHour__hour=current_time.hour)
    for mssg in push_messages:
        users = None
        if(mssg.segment == '1'):
            users = User.objects.filter(groups__name='Client')
        devices = FCMDevice.objects.filter(user__in = users)
        devices.send_message(title= mssg.title, body= mssg.message)



def sms_job():
    current_time = datetime.now()
    log = CronLog(
        job = 'sms_job',
        executionTime = current_time
    )
    log.save()
    sms_messages = SMSMessage.objects.filter(is_active = True, initDate__lte = current_time, finishDate__gte = current_time, sendHour__hour=current_time.hour)
    for mssg in sms_messages:
        users = None
        if(mssg.segment == '1'):
            users = User.objects.filter(groups__name='Client')
        phones = []
        if len(users) > 0:
            for user in users:
                phoneAppend = {"msisdn": "57" + str(user.phone)}
                if phoneAppend not in phones :
                    phones.append(phoneAppend)
                sendMarketingSMS(phones,mssg.message)
